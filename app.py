from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import json

app = Flask(__name__)
api = Api(app)

USER_FILE = 'users.txt'


def get_user_list():
    try:
        with open(USER_FILE) as file:
            return json.load(file)
    except:
        abort(404, error='Can not read user file')

def put_user_list(users):
    try:
        with open(USER_FILE, 'w') as file:
            json.dump(users, file)
    except:
        abort(404, error='Can not write user dict to file')


def abort_if_user_doesnt_exist(user_id):
    users = get_user_list()
    if user_id not in users:
        abort(404, error="User {} doesn't exist".format(user_id))


parser = reqparse.RequestParser()
parser.add_argument('name')


# shows a single user by id and lets you delete one
class User(Resource):
    def get(self, user_id):
        abort_if_user_doesnt_exist(user_id)
        users = get_user_list()
        return users[user_id]

    def delete(self, user_id):
        abort_if_user_doesnt_exist(user_id)
        users = get_user_list()
        del users[user_id]
        put_user_list(users)
        return '', 204

    def put(self, user_id):
        args = parser.parse_args()
        task = {'name': args['name']}
        users = get_user_list()
        users[user_id] = task
        put_user_list(users)
        return task, 201


# shows a list of all users, and lets you POST to add new one
class UserList(Resource):
    def get(self):
        users = get_user_list()
        return users

    def post(self):
        args = parser.parse_args()
        users = get_user_list()
        user_id = int(max(users.keys()).lstrip('user')) + 1
        user_id = 'user%i' % user_id
        users[user_id] = {'name': args['name']}
        put_user_list(users)
        return users[user_id], 201


api.add_resource(UserList, '/users')
api.add_resource(User, '/users/<user_id>')


if __name__ == '__main__':
    app.run(debug=True)
